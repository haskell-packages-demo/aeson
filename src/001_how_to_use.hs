import      "aeson" Data.Aeson
    ( encode )
import      "aeson" Data.Aeson.Types
    ( defaultOptions
    , FromJSON
    , genericToEncoding
    , toEncoding
    , ToJSON
    )
import safe "bytestring" Data.ByteString.Lazy.Char8 qualified as CL
    ( putStrLn )
import safe "base" Data.Function
    ( ($) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
import safe "text" Data.Text
    ( Text )
-- import safe "text" Data.Text.IO ()
import safe "base" GHC.Generics
    ( Generic )
import safe "base" Text.Show
    ( Show )
import safe "base" System.IO
    ( IO )

type Person :: Type
data Person = Person {
      name :: Text
    , age  :: Int
    } deriving stock (Generic, Show)

instance ToJSON Person where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Person
    -- No need to provide a parseJSON implementation.

main :: IO ()
main = CL.putStrLn
  $ encode (Person {name = "Joe", age = 12})

{-
Based on:
https://hackage.haskell.org/package/aeson/docs/Data-Aeson.html
Version: 2.2.2.0
-}